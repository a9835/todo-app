import { Component, Output, EventEmitter } from '@angular/core';
import {Todo} from '../../Models/Todo'

@Component({
  selector: 'app-add-todo-form',
  templateUrl: './add-todo-form.component.html',
  styleUrls: ['./add-todo-form.component.css']
})
export class AddTodoFormComponent {
@Output() newTodoEvent = new EventEmitter<Todo>();

InputTodo:string="";

addTodo (){
  const todo:Todo = {
    content: this.InputTodo,
    completed: false
  };
  this.newTodoEvent.emit(todo);
  this.InputTodo = "";
  }
}

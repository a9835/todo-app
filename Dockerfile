FROM node:16-alpine as build
WORKDIR /app
COPY . .
RUN npm install
RUN npm install -g @angular/cli
RUN npm run build

FROM nginx:alpine
COPY --from=build /app/dist/to-do-app-day1 /usr/share/nginx/html

